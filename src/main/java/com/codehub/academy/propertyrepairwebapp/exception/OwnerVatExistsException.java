package com.codehub.academy.propertyrepairwebapp.exception;

import com.codehub.academy.propertyrepairwebapp.utils.GlobalAttributes;

public class OwnerVatExistsException extends RuntimeException {

    public OwnerVatExistsException() {
        super(GlobalAttributes.OWNER_VAT_EXISTS_MESSAGE);
    }
}
