package com.codehub.academy.propertyrepairwebapp.exception;

import com.codehub.academy.propertyrepairwebapp.utils.GlobalAttributes;

public class OwnerEmailExistsException extends RuntimeException {

    public OwnerEmailExistsException() {
        super(GlobalAttributes.OWNER_EMAIL_EXISTS_MESSAGE);
    }
}
