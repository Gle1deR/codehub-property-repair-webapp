package com.codehub.academy.propertyrepairwebapp.controller;

import com.codehub.academy.propertyrepairwebapp.domain.Owner;
import com.codehub.academy.propertyrepairwebapp.exception.OwnerEmailExistsException;
import com.codehub.academy.propertyrepairwebapp.exception.OwnerVatExistsException;
import com.codehub.academy.propertyrepairwebapp.forms.UpdateUserForm;
import com.codehub.academy.propertyrepairwebapp.service.OwnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class EditOwnerController {

    private static final String OWNER_FORM = "editOwnerForm";

    @Autowired
    OwnerService ownerService;

    @GetMapping(value = "/admin/edit/{id}/owner")
    public String editOwnerPage(@PathVariable Long id, Model model) {

        Owner foundOwner = ownerService.findById(id);
        model.addAttribute("owner", foundOwner);
        model.addAttribute(OWNER_FORM, new UpdateUserForm());

        return "edit-owner";
    }

    @PostMapping({"/admin/owner/{id}/edit"})
    public String editOwner(@PathVariable Long id, @Valid @ModelAttribute(OWNER_FORM)
            UpdateUserForm editUserForm, BindingResult result, ModelMap model) {

        if (result.hasErrors()) {
            Owner foundOwner = ownerService.findById(id);
            model.addAttribute("owner", foundOwner);
            return "edit-owner";
        }

        try {
            ownerService.updateOwner(editUserForm, id);
        } catch (OwnerEmailExistsException ex) {
            model.addAttribute("emailExists", ex.getMessage());
            Owner foundOwner = ownerService.findById(id);
            model.addAttribute("owner", foundOwner);
            return "edit-owner";
        } catch (OwnerVatExistsException ex) {
            model.addAttribute("vatExists", ex.getMessage());
            Owner foundOwner = ownerService.findById(id);
            model.addAttribute("owner", foundOwner);
            return "edit-owner";
        }

        return "redirect:/admin/owners";
    }
}
