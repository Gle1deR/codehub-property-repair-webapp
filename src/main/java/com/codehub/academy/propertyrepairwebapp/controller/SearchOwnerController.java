package com.codehub.academy.propertyrepairwebapp.controller;

import com.codehub.academy.propertyrepairwebapp.domain.Owner;
import com.codehub.academy.propertyrepairwebapp.forms.OwnerSearchForm;
import com.codehub.academy.propertyrepairwebapp.service.OwnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class SearchOwnerController {

    @Autowired
    private OwnerService ownerService;

    @GetMapping("/admin/search-owner")
    public String searchTest(ModelMap modelMap) {
        modelMap.addAttribute("ownerSearchForm", new OwnerSearchForm());
        return "search-owner";
    }

    @PostMapping("/admin/search-owner")
    public String searchTest(@ModelAttribute("ownerSearchForm")
                                     OwnerSearchForm ownerSearchForm, ModelMap modelMap) {

        Owner owner = ownerService.searchOwner(ownerSearchForm);

        if (owner != null) {
            List<Owner> owners = new ArrayList<>();
            owners.add(owner);
            modelMap.addAttribute("owners", owners);
        }
        return "show-owners";
    }
}
