package com.codehub.academy.propertyrepairwebapp.controller;

import com.codehub.academy.propertyrepairwebapp.domain.Repair;
import com.codehub.academy.propertyrepairwebapp.forms.RepairForm;
import com.codehub.academy.propertyrepairwebapp.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class CreateRepairController {
    private static final String REPAIR_FORM = "createRepairForm";

    @Autowired
    private RepairService repairService;

    @GetMapping(value = "/admin/create-repair")
    public String registerRepair(ModelMap model) {
        // Get all existing vats of owners in DB
        List<String> existingOwnerVats = repairService.getOwnerVats();

        model.addAttribute("existingOwnerVats", existingOwnerVats);
        model.addAttribute(REPAIR_FORM, new RepairForm());
        return "create-repair";
    }

    @PostMapping({"/admin/create-repair"})
    public String registerRepair(@Valid @ModelAttribute(REPAIR_FORM)
                                         RepairForm repairForm, BindingResult result, ModelMap model) {

        if (result.hasErrors()) {
            List<String> existingOwnerVats = repairService.getOwnerVats();

            model.addAttribute("existingOwnerVats", existingOwnerVats);
            model.addAttribute("errorMessage", "an error occurred");
            return "create-repair";
        }

        Repair repair = repairService.createRepair(repairForm);
        List<Repair> repairs = repairService.findAll();
        model.addAttribute("repairs", repairs);

        return "show-repairs";
    }
}
