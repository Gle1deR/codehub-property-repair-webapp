package com.codehub.academy.propertyrepairwebapp.controller;

import com.codehub.academy.propertyrepairwebapp.domain.Owner;
import com.codehub.academy.propertyrepairwebapp.service.OwnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class OwnerController {
    @Autowired
    OwnerService ownerService;

    @GetMapping({"/admin/owners"})
    public String displayOwners(Model model) {
        List<Owner> owners = ownerService.findAll();
        model.addAttribute("owners", owners);

        return "show-owners";
    }
}
