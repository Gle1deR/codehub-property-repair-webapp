package com.codehub.academy.propertyrepairwebapp.controller;

import com.codehub.academy.propertyrepairwebapp.service.OwnerService;
import com.codehub.academy.propertyrepairwebapp.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class DeleteController {
    @Autowired
    private RepairService repairService;

    @Autowired
    OwnerService ownerService;

    @PostMapping(value = "/admin/repairs/{id}/delete")
    public String deleteRepair(@PathVariable Long id) {
        repairService.deleteById(id);
        return "redirect:/admin/repairs";

    }

    @PostMapping(value = "/admin/owners/{id}/delete")
    public String deleteOwner(@PathVariable Long id) {
        ownerService.deleteOwner(id);
        return "redirect:/admin/owners";

    }

}

