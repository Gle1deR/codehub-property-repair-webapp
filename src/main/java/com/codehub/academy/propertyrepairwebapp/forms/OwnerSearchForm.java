package com.codehub.academy.propertyrepairwebapp.forms;

public class OwnerSearchForm {

    private String vat;

    private String email;

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "OwnerSearchForm{" +
                "vat='" + vat + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
