package com.codehub.academy.propertyrepairwebapp.forms;

import com.codehub.academy.propertyrepairwebapp.utils.GlobalAttributes;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class RepairForm {

    @NotNull(message = GlobalAttributes.DATE_MESSAGE)
    @DateTimeFormat(pattern = GlobalAttributes.DATE_PATTERN)
    private String date;

    private String repairStatus;

    private String repairType;

    @Size(min = GlobalAttributes.ADDRESS_SIZE, message = GlobalAttributes.ADDRESS_SIZE_MESSAGE)
    private String address;

    @NotNull(message = "Cost can't be empty")
    private int cost;

    private String description;

    @Pattern(regexp = GlobalAttributes.VAT_PATTERN, message = GlobalAttributes.VAT_LENGTH_MESSAGE)
    @Size(min = GlobalAttributes.VAT_LENGTH, max = GlobalAttributes.VAT_LENGTH, message = GlobalAttributes.VAT_EMPTY_MESSAGE)
    private String vat;

    private String firstName;
    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRepairStatus() {
        return repairStatus;
    }

    public void setRepairStatus(String repairStatus) {
        this.repairStatus = repairStatus;
    }

    public String getRepairType() {
        return repairType;
    }

    public void setRepairType(String repairType) {
        this.repairType = repairType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    @Override
    public String toString() {
        return "RegisterRepairForm{" +
                "date=" + date +
                ", repairStatus=" + repairStatus +
                ", repairType=" + repairType +
                ", address='" + address + '\'' +
                ", cost=" + cost +
                ", description='" + description + '\'' +
                ", vat='" + vat + '\'' +
                '}';
    }
}
