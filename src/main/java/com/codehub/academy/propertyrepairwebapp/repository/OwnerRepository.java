package com.codehub.academy.propertyrepairwebapp.repository;

import com.codehub.academy.propertyrepairwebapp.domain.Owner;
import com.codehub.academy.propertyrepairwebapp.domain.Repair;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface OwnerRepository extends JpaRepository<Owner, Long> {
    Optional<Owner> findOwnerByVat(String vat);
    List<Owner> findAll();
    Page<Owner> findAll(Pageable pageable);
    Owner save(Owner owner);
    void deleteById(Long id);
    Owner findByEmail(String email);
    Optional<Owner> findByVatAndEmail(String vat, String email);
}
