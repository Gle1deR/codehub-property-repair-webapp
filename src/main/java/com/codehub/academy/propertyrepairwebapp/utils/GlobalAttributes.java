package com.codehub.academy.propertyrepairwebapp.utils;

public class GlobalAttributes {

    public static final String ERROR_MESSAGE = "errorMessage";
    public static final String MESSAGE = "message";
    public static final String REDIRECT_MESSAGE = "redirectMsg";
    public static final String MESSAGE_LIST = "messages";

    // Patterns
    public static final String VAT_PATTERN = "^[0-9]{9}$";
    public static final int VAT_LENGTH = 9;
    public static final String NAME_PATTERN = "^[A-Za-z]*$";
    public static final int NAME_SIZE = 3;
    public static final String EMAIL_PATTERN = "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{1,63}$";
    public static final String PASSWORD_PATTERN = "^[a-zA-Z0-9@#$%^&]*$";
    public static final String EDITED_PASSWORD_PATTERN = "[a-zA-Z0-9@#$%^&]*";
    public static final int PASSWORD_SIZE = 5;
    public static final int ADDRESS_SIZE = 3;
    public static final String DATE_PATTERN = "yyyy-MM-dd";
    public static final String PHONE_PATTERN = "[0-9]*";

    // Messages
    public static final String DATE_MESSAGE = "Please choose a date";
    public static final String DESCRIPTION_MESSAGE = "Description must be at least 5 characters long";
    public static final String VAT_PATTERN_MESSAGE = "VAT must contain 9 numbers";
    public static final String PHONE_PATTERN_MESSAGE = "Phone number must contain only digits";
    public static final String VAT_LENGTH_MESSAGE = "VAT requires 9 digits";
    public static final String VAT_EMPTY_MESSAGE = "VAT can't be empty";
    public static final String NAME_PATTERN_MESSAGE = "Please use only letters";
    public static final String NAME_SIZE_MESSAGE = "At least 3 letters required";
    public static final String ADDRESS_SIZE_MESSAGE = "Address has to be at least 3 characters";
    public static final String EMAIL_PATTERN_MESSAGE = "Please use a valid email";
    public static final String PASSWORD_PATTERN_MESSAGE = "Password can contain numbers, characters and @#$%^&";
    public static final String EDITED_PASSWORD_PATTERN_MESSAGE = "Password can contain numbers, characters and @#$%^& and must be more than 5 letters";
    public static final String PASSWORD_SIZE_MESSAGE = "Password must be more than 5 characters";
    public static final String COST_MESSAGE = "Use a number in the cost field";
    public static final String OWNER_VAT_EXISTS_MESSAGE = "User with this VAT already exists";
    public static final String OWNER_EMAIL_EXISTS_MESSAGE = "User with this email already exists";
}
