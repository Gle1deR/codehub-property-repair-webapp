package com.codehub.academy.propertyrepairwebapp.enums;

public enum OwnerType {
    ADMIN("ADMIN"),
    USER("USER");

    private String type;

    OwnerType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
