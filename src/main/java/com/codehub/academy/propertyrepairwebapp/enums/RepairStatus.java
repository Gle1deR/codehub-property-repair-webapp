package com.codehub.academy.propertyrepairwebapp.enums;

public enum RepairStatus {

    PENDING("Pending"),
    ONGOING("Ongoing"),
    COMPLETE("Complete");

    private String status;

    RepairStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
