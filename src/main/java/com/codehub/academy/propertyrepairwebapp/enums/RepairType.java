package com.codehub.academy.propertyrepairwebapp.enums;

public enum RepairType {

    PAINTING("Painting"),
    INSULATION("Insulation"),
    FRAMES("Frames"),
    PLUMBING("Plumbing"),
    ELECTRICAL_SERVICES("Electrical Services");

    private String type;

    RepairType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
