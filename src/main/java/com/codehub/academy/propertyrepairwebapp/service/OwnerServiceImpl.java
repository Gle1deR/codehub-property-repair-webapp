package com.codehub.academy.propertyrepairwebapp.service;

import com.codehub.academy.propertyrepairwebapp.domain.Owner;
import com.codehub.academy.propertyrepairwebapp.exception.OwnerEmailExistsException;
import com.codehub.academy.propertyrepairwebapp.exception.OwnerVatExistsException;
import com.codehub.academy.propertyrepairwebapp.forms.OwnerSearchForm;
import com.codehub.academy.propertyrepairwebapp.forms.RegisterUserForm;
import com.codehub.academy.propertyrepairwebapp.forms.UpdateUserForm;
import com.codehub.academy.propertyrepairwebapp.mappers.OwnerFormToOwnerMapper;
import com.codehub.academy.propertyrepairwebapp.mappers.OwnerToOwnerModelMapper;
import com.codehub.academy.propertyrepairwebapp.model.OwnerModel;
import com.codehub.academy.propertyrepairwebapp.repository.OwnerRepository;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OwnerServiceImpl implements OwnerService {
    @Autowired
    private OwnerRepository ownerRepository;

    @Autowired
    private OwnerService ownerService;

    @Autowired
    private OwnerToOwnerModelMapper mapper;

    @Autowired
    private OwnerFormToOwnerMapper ownerMapper;

    @Override
    public Optional<Owner> findOwnerByVat(String vat) {
        return ownerRepository.findOwnerByVat(vat);
    }

    @Override
    public Owner findById(Long id) {
        return ownerRepository.findById(id).get();
    }

    @Override
    public List<Owner> getOwner(String vat, String email) {
        return null;
    }

    @Override
    public Owner getOwnerById(Long id) {
        return null;
    }

    @Override
    public void deleteOwner(Long id) {
        ownerRepository.deleteById(id);
    }

    @Override
    public Owner updateOwner(UpdateUserForm registerUserForm, Long id) {
        Owner editedOwner = ownerMapper.toOwner(registerUserForm);
        Owner originalOwner = ownerRepository.findById(id).get();

        originalOwner.setFirstName(editedOwner.getFirstName());
        originalOwner.setLastName(editedOwner.getLastName());
        originalOwner.setEmail(editedOwner.getEmail());
        originalOwner.setVat(editedOwner.getVat());
        originalOwner.setAddress(editedOwner.getAddress());
        originalOwner.setOwnerType(editedOwner.getOwnerType());
        originalOwner.setPropertyType(editedOwner.getPropertyType());
        originalOwner.setPhoneNumber(editedOwner.getPhoneNumber());
        if (!editedOwner.getPassword().isEmpty()) {
            originalOwner.setPassword(editedOwner.getPassword());
        }

        try {
            ownerRepository.save(originalOwner);
        } catch (DataIntegrityViolationException ex) {
            LoggerFactory.getLogger(this.getClass()).info("-------------------------" + ex.getMessage() +
                    "\n------------------" +ex.getMostSpecificCause().getMessage());
            if (ex.getMostSpecificCause().getMessage().contains("@")) {
                throw new OwnerEmailExistsException();
            } else {
                throw new OwnerVatExistsException();
            }
        }

        return originalOwner;
    }

    @Override
    public void create(OwnerModel ownerModel) {

    }

    @Override
    public List<Owner> findAll() {
        return ownerRepository.findAll();
    }

    @Override
    public Owner findByEmail(String email) {
        return ownerRepository.findByEmail(email);
    }

    @Override
    public Owner createOwner(RegisterUserForm ownerForm) {
        Owner owner = ownerMapper.toOwner(ownerForm);

        try {
            ownerRepository.save(owner);
        } catch (DataIntegrityViolationException ex) {
            if (ex.getMostSpecificCause().getMessage().contains("@")) {
                throw new OwnerEmailExistsException();
            } else {
                throw new OwnerVatExistsException();
            }
        }

        return owner;
    }

    @Override
    public Owner searchOwner(OwnerSearchForm ownerSearchForm) {
        String vat = ownerSearchForm.getVat();
        String email = ownerSearchForm.getEmail();

        Optional<Owner> owner = Optional.empty();
        if (!vat.isEmpty()) {
            if (!email.isEmpty()) {
                owner = ownerRepository.findByVatAndEmail(vat, email);
            } else {
                owner = ownerRepository.findOwnerByVat(vat);
            }
        } else {
            if (!email.isEmpty()) {
                owner = Optional.of(ownerRepository.findByEmail(email));
            }
        }
        return owner.orElse(null);
    }
}
