package com.codehub.academy.propertyrepairwebapp.service;

import com.codehub.academy.propertyrepairwebapp.domain.Owner;
import com.codehub.academy.propertyrepairwebapp.domain.Repair;
import com.codehub.academy.propertyrepairwebapp.enums.RepairStatus;
import com.codehub.academy.propertyrepairwebapp.forms.RepairForm;
import com.codehub.academy.propertyrepairwebapp.forms.SearchRepairForm;
import com.codehub.academy.propertyrepairwebapp.mappers.RepairFormToRepairMapper;
import com.codehub.academy.propertyrepairwebapp.mappers.RepairToRepairModelMapper;
import com.codehub.academy.propertyrepairwebapp.model.RepairModel;
import com.codehub.academy.propertyrepairwebapp.repository.RepairRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RepairServiceImpl implements RepairService {

    @Autowired
    RepairRepository repairRepository;

    @Autowired
    OwnerService ownerService;

    @Autowired
    private RepairToRepairModelMapper mapper;

    @Autowired
    private RepairFormToRepairMapper repairMapper;

    @Override
    public Repair createRepair(RepairForm repairForm) {
        // get from DB owner's information
        Owner owner = ownerService.findOwnerByVat(repairForm.getVat()).orElseThrow();

        Repair repair = repairMapper.toRepair(repairForm);
        repair.setOwner(owner); // set in repair object all of the owner's information

        return repairRepository.save(repair);
    }

    @Override
    public Repair updateRepair(RepairForm repairForm, Long id) {
        Repair editedRepair = repairMapper.toRepair(repairForm);
        Repair originalRepair = repairRepository.findById(id).get();

        originalRepair.setRepairDate(editedRepair.getRepairDate());
        originalRepair.setRepairStatus(editedRepair.getRepairStatus());
        originalRepair.setRepairType(editedRepair.getRepairType());
        originalRepair.setDescription(editedRepair.getDescription());
        originalRepair.setCost(editedRepair.getCost());
        originalRepair.setAddress(editedRepair.getAddress());

        Optional<Owner> ownerOptional = ownerService.findOwnerByVat(editedRepair.getOwner().getVat());
        if(!ownerOptional.isEmpty()){
            Owner owner = ownerOptional.get();
            originalRepair.setOwner(owner);
        }

        return repairRepository.save(originalRepair);
    }

    @Override
    public Optional<RepairModel> findRepair(Long id) {
        return repairRepository
                .findById(id)
                .map(repair -> mapper.mapToRepairModel(repair));
    }

    @Override
    public Repair findById(Long id) {
        return repairRepository.findById(id).get();
    }

    @Override
    public void deleteById(Long id) {
        repairRepository.deleteById(id);
    }

    @Override
    public List<Repair> findAll() {
        return repairRepository
                .findAll();
//                .stream()
//                .map(repair -> mapper.mapToRepairModel(repair))
//                .collect(Collectors.toList());
    }

    @Override
    public List<Repair> findRepairsByVat(String vat) {
        return repairRepository
                .findByOwnerVat(vat);
    }

    @Override
    public List<String> getOwnerVats() {
        return ownerService.findAll()
                .stream()
                .map(owner -> owner.getVat())
                .collect(Collectors.toList());
    }

    @Override
    public List<Repair> searchRepair(SearchRepairForm searchRepairForm) {
        String startDate = searchRepairForm.getStartDate();
        String endDate = searchRepairForm.getEndDate();
        String vat = searchRepairForm.getVat();

        List<Repair> dateRepairs = new ArrayList<>();

        if (startDate.isEmpty() && endDate.isEmpty() && vat.isEmpty()) {
            dateRepairs = findAll();
        }

        if (!startDate.isEmpty()) {
            LocalDate startLocalDate = LocalDate.parse(startDate,
                    DateTimeFormatter.ofPattern("yyyy-MM-dd"));

            if (!endDate.isEmpty()) {
                LocalDate endLocalDate = LocalDate.parse(endDate,
                        DateTimeFormatter.ofPattern("yyyy-MM-dd"));

                dateRepairs = repairRepository.findByRepairDateBetween(startLocalDate, endLocalDate);
            } else {
                dateRepairs = repairRepository.findByRepairDateGreaterThanEqual(startLocalDate);
            }
        } else {
            if (!endDate.isEmpty()) {
                LocalDate endLocalDate = LocalDate.parse(endDate,
                        DateTimeFormatter.ofPattern("yyyy-MM-dd"));

                dateRepairs = repairRepository.findByRepairDateLessThanEqual(endLocalDate);
            }
        }

        if (!vat.isEmpty()) {
            Optional<Owner> ownerOptional = ownerService.findOwnerByVat(vat);
            if (!ownerOptional.isEmpty()) {
                List<Repair> vatRepairs = repairRepository.findByOwnerId(ownerOptional.get().getId());

                dateRepairs = dateRepairs.stream()
                        .filter(dateRepair -> dateRepair.getOwner().getVat().equals(vat))
                        .distinct()
                        .collect(Collectors.toList());

                if (startDate.isEmpty() && endDate.isEmpty()) {
                    return vatRepairs;
                }
            }
        }
        return dateRepairs;
    }

    @Override
    public List<Repair> findOngoingRepairs() {
        return repairRepository.findByRepairStatusOrderByRepairDate(RepairStatus.ONGOING);
    }

    @Override
    public List<Repair> findOwnerRepairs(Long id) {
        return repairRepository.findByOwnerId(id);
    }
}
