package com.codehub.academy.propertyrepairwebapp.service;

import com.codehub.academy.propertyrepairwebapp.domain.Owner;
import com.codehub.academy.propertyrepairwebapp.forms.OwnerSearchForm;
import com.codehub.academy.propertyrepairwebapp.forms.RegisterUserForm;
import com.codehub.academy.propertyrepairwebapp.forms.UpdateUserForm;
import com.codehub.academy.propertyrepairwebapp.model.OwnerModel;

import java.util.List;
import java.util.Optional;

public interface OwnerService {
    Optional<Owner> findOwnerByVat(String vat);

    Owner findById(Long id);

    List<Owner> getOwner(String vat, String email);

    Owner getOwnerById(Long id);

    void deleteOwner(Long id);

    Owner updateOwner(UpdateUserForm registerUserForm, Long id);

    void create(OwnerModel ownerModel);

    List<Owner> findAll();

    Owner findByEmail(String email);

    Owner createOwner(RegisterUserForm registerUserForm);

    Owner searchOwner(OwnerSearchForm ownerSearchForm);
}
