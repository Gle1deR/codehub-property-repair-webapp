package com.codehub.academy.propertyrepairwebapp.service;

import com.codehub.academy.propertyrepairwebapp.domain.Owner;
import com.codehub.academy.propertyrepairwebapp.model.LoginResponse;
import com.codehub.academy.propertyrepairwebapp.repository.OwnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    OwnerRepository ownerRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // here we would search into the repo for the owner.
        // for not we are just going to send always a successful response.
        Owner owner = ownerRepository.findByEmail(username);

/*        List<SimpleGrantedAuthority> authorization = Collections.singletonList(new SimpleGrantedAuthority("ADMIN"));
        CharSequence password = "password";*/

        return new LoginResponse(owner.getEmail(), owner.getPassword(),
                Arrays.asList(new SimpleGrantedAuthority(owner.getOwnerType().getType())));
    }
}
